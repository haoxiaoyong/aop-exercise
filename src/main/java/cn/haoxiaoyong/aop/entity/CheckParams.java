package cn.haoxiaoyong.aop.entity;

/**
 * Created by haoxiaoyong on 2019/2/26 下午 9:29
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class CheckParams {

    private String appid;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public CheckParams(String appid) {
        this.appid = appid;
    }

    public CheckParams() {
    }
}
