package cn.haoxiaoyong.aop.controller;

import cn.haoxiaoyong.aop.annotation.CheckAppid;
import cn.haoxiaoyong.aop.require.CheckAppIdEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by haoxiaoyong on 2019/2/25 下午 10:04
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
@RestController
@RequestMapping(value = "hello")
public class HelloController {

    @CheckAppid
    @RequestMapping(value = "/aop")
    public String hello(@RequestBody CheckAppIdEntity checkAppIdEntity){
        System.out.println(checkAppIdEntity.getName());
        return "你好！ "+checkAppIdEntity.getName();
    }
}
