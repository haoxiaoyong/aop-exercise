package cn.haoxiaoyong.aop.require;

import cn.haoxiaoyong.aop.entity.CheckParams;

/**
 * Created by haoxiaoyong on 2019/2/26 下午 9:34
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
public class CheckAppIdEntity extends CheckParams {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CheckAppIdEntity(String appid, String name) {
        super(appid);
        this.name = name;
    }

    public CheckAppIdEntity() {
    }
}
