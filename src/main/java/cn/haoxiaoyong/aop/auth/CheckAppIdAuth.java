package cn.haoxiaoyong.aop.auth;

import org.springframework.stereotype.Component;

/**
 * Created by haoxiaoyong on 2019/2/26 下午 9:39
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
@Component
public class CheckAppIdAuth {

    private String appid;

    public CheckAppIdAuth(String appid) {
        this.appid = appid;
    }

    public CheckAppIdAuth() {
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public boolean checkAppId() {
        if ("haoxy1234".equals(this.appid)) {
            return true;
        }
        return false;
    }
}
