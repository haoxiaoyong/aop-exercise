package cn.haoxiaoyong.aop.monitor;

import cn.haoxiaoyong.aop.auth.CheckAppIdAuth;
import cn.haoxiaoyong.aop.entity.CheckParams;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by haoxiaoyong on 2019/2/26 下午 9:10
 * e-mail: hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * Blog: www.haoxiaoyong.cn
 */
@Aspect
@Component
public class CheckAppidInfo {

    private static Logger logger = LoggerFactory.getLogger(CheckAppidInfo.class);
    @Autowired
    private CheckAppIdAuth checkAppIdAuth;

    @Pointcut("@annotation(cn.haoxiaoyong.aop.annotation.CheckAppid)")
    public void CheckAppidExercise() {
    }

    @Before("CheckAppidExercise()")
    public void CheckAppIdDetail(JoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        HttpServletResponse httpServletResponse = null;
        LinkedList<Object> params = new LinkedList<>();
        for (Object arg : args) {
            if (arg instanceof CheckParams) {
                try {
                    this.checkAppIdAuth.setAppid(((CheckParams) arg).getAppid());
                } catch (Exception e) {
                    this.checkAppIdAuth.setAppid("null");
                }
            }
            if (arg instanceof HttpServletResponse) {
                httpServletResponse = (HttpServletResponse) arg;
            } else {
                if (!(arg instanceof HttpServletRequest)) {
                    params.add(arg);
                }
            }
        }
        if(checkAppIdAuth.checkAppId()){
            logger.info("验证AppId存在.....success");
           // proceedingJoinPoint.proceed();

        }else {
            logger.warn("经过检验AppId不存在,请联系运维人员....");

        }

    }


}
